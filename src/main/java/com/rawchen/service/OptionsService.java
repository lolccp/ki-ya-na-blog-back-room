package com.rawchen.service;

import com.rawchen.domain.Options;
import com.rawchen.mapper.OptionsMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
@Service
public class OptionsService {
	@Resource
	private OptionsMapper optionsMapper;

	public String selectValueByName(String name){
		return optionsMapper.selectValueByName(name);
	}

	public int updateOptions(Options options){
		return optionsMapper.updateByPrimaryKeyWithBLOBs(options);
	}
}
