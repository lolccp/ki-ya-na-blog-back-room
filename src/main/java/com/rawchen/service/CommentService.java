package com.rawchen.service;

import com.rawchen.domain.Comment;
import com.rawchen.mapper.CommentMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class CommentService {
	@Resource
	private CommentMapper commentMapper;
	public List<Comment> selectCommentListByContentId(int id){
		return commentMapper.selectCommentListByContentId(id);
	}

	public String selectCommentAuthorById(Integer coid){
		return commentMapper.selectCommentAuthorById(coid);
	}

	public int insert(Comment comment){
		return commentMapper.insert(comment);
	}

	public List<Comment> selectAllComment(){
		return commentMapper.selectAllCommment();
	}

	public int deleteByPrimaryKey(int coid){
		return commentMapper.deleteByPrimaryKey(coid);
	}

	public int deleteSelectComment(String[] coids){
		try {
			if (coids != null && coids.length > 0) {
				for (String coid: coids) {
					commentMapper.deleteByPrimaryKey(Integer.parseInt(coid));
				}
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return 0;
		}
		return 1;
	}

	public List<Comment> selectCommentListWithUserId(int userId){
		return commentMapper.selectCommentListWithUserId(userId);
	}
}
