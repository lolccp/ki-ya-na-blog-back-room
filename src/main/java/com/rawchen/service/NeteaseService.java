package com.rawchen.service;

import com.rawchen.domain.Netease;
import com.rawchen.mapper.NeteaseMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class NeteaseService {
    @Resource
    private NeteaseMapper neteaseMapper;

    /**
     * 查询网易云歌单id
     * @return
     */
    public Netease showAllandID(){
        return neteaseMapper.selectByPrimaryKey();
    }

    public int updateNetease(Netease netease){
        return neteaseMapper.updateByPrimaryKeySelective(netease);
    }
}
