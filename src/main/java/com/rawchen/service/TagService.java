package com.rawchen.service;

import com.rawchen.domain.Tag;
import com.rawchen.mapper.TagMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class TagService {
	@Resource
	private TagMapper tagMapper;
	public List<Tag> selectAllTag(){
		return tagMapper.selectAllTag();
	}

	public List<Tag> fuzzyQueryTag(String tagName){
		return tagMapper.fuzzyQueryTag(tagName);
	}

	public int findTagIdByName(String s){
		return tagMapper.findTagIdByName(s);
	}

	public int insert(Tag tag){
		return tagMapper.insert(tag);
	}

	public Tag findTagById(int tagId){
		return tagMapper.findTagById(tagId);
	}

	public int selectCountOfTag(){
		int num = tagMapper.selectCountOfTag();
		if (num!=0) {
			return num;
		}else {
			return 0;
		}
	}

	public int updateTagCount(int tagId,int count){
		return tagMapper.updateTagCount(tagId,count);
	}

	public int updateTagName(Tag tag){
		return tagMapper.updateTagName(tag);
	}

	public int deleteTag(int tid){
		return tagMapper.deleteByPrimaryKey(tid);
	}
}
