package com.rawchen.service;

import com.rawchen.domain.Category;
import com.rawchen.mapper.CategoryMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class CategoryService {
	@Resource
	private CategoryMapper categoryMapper;

	public List<Category> selectAllCategory(){
		return categoryMapper.selectAllCategory();
	}

	public Integer selectCategoryBySlug(String blogCategory){
		return categoryMapper.selectCategoryBySlug(blogCategory);
	}

	public Category selectByPrimaryKey(Integer cgid){
		return categoryMapper.selectByPrimaryKey(cgid);
	}

	public int selectCountOfCategory(){
		int num = categoryMapper.selectCountOfCategory();
		if (num!=0) {
			return num;
		}else {
			return 0;
		}
	}

	public String selectCategoryNameById(Integer cgid){
		return categoryMapper.selectCategoryNameById(cgid);
	}

	public int updateCategory(Category category){
		return categoryMapper.updateByPrimaryKey(category);
	}

	public int deleteCategory(int parseInt){
		return categoryMapper.deleteByPrimaryKey(parseInt);
	}

	public int insertCategory(Category category){
		return categoryMapper.insert(category);
	}
}
