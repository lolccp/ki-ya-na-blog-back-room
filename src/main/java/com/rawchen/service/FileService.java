package com.rawchen.service;

import com.rawchen.domain.MyFile;
import com.rawchen.mapper.FileMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class FileService {
	@Resource
	private FileMapper fileMapper;

	public List<MyFile> selectAllFile(){
		return fileMapper.selectAllFile();
	}

	public List<MyFile> selectFileListWithUid(int userId){
		return fileMapper.selectFileListWithUid(userId);
	}

	public int insert(MyFile newFile){
		return fileMapper.insert(newFile);
	}

	public int deleteByPrimaryKey(int fid){
		return fileMapper.deleteByPrimaryKey(fid);
	}

	public MyFile selectFileByFid(int fid){
		return fileMapper.selectByPrimaryKey(fid);
	}
}
