package com.rawchen.service;

import com.rawchen.domain.Log;
import com.rawchen.domain.dto.SevenDayLog;
import com.rawchen.mapper.LogMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
@Service
public class LogService {
	@Resource
	private LogMapper logMapper;
	public int insert(Log log){
		return logMapper.insert(log);
	}

	public List<Integer> selectYesterdayPvUvIndexGuestbook(){
		List<Integer> y = new ArrayList<>();
		y.add(logMapper.selectYesterdayPv());
		y.add(logMapper.selectYesterdayUv());
		y.add(logMapper.selectYesterdayAccessByApi("/"));
		y.add(logMapper.selectYesterdayAccessByApi("/guestbook"));
		y.add(logMapper.selectYesterdayAccessLikeApi("/articles/"));
		return y;
	}

	public List<Integer> selectTodayPvUvIndexGuestbook(){
		List<Integer> t = new ArrayList<>();
		t.add(logMapper.selectTodayPv());
		t.add(logMapper.selectTodayUv());
		t.add(logMapper.selectTodayAccessByApi("/"));
		t.add(logMapper.selectTodayAccessByApi("/guestbook"));
		t.add(logMapper.selectTodayAccessLikeApi("/articles/"));
		return t;
	}

	public List<Integer> selectSevenDaysPv(){
		List<Integer> l = new ArrayList<>();
		List<SevenDayLog> logs = logMapper.selectSevenDaysPv();
		for (SevenDayLog log : logs) {
			l.add(log.getAccessValue());
		}
		return l;
	}

	public List<Integer> selectSevenDaysUv(){
		List<Integer> l = new ArrayList<>();
		List<SevenDayLog> logs = logMapper.selectSevenDaysUv();
		for (SevenDayLog log : logs) {
			l.add(log.getAccessValue());
		}
		return l;
	}
}
