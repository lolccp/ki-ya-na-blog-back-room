package com.rawchen.service;

import com.rawchen.domain.Content;
import com.rawchen.mapper.CommentMapper;
import com.rawchen.mapper.ContentMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class ContentService {
	@Resource
	private ContentMapper contentMapper;
	@Resource
	private CommentMapper commentMapper;

	public Content selectByPrimaryKey(Integer cid){
		return contentMapper.selectByPrimaryKey(cid);
	}

	public List<Content> selectAllContent(){
		return contentMapper.selectAllContent();
	}

	public int selectNumberOfArticles(){
		int num = contentMapper.selectNumberOfArticles();
		if (num!=0) {
			return num;
		}else {
			return 0;
		}
	}

	public List<Content> selectRecommendContent(){
		return contentMapper.selectRecommendContent();
	}

	public int insert(Content content){
		return contentMapper.insert(content);
	}

	public Content findContentBySlugName(String slugName){
		return contentMapper.findContentBySlugName(slugName);
	}

	public int selectContentCountByCgid(Integer cgid){
		int num = contentMapper.selectContentCountByCgid(cgid);
		if (num!=0) {
			return num;
		}else {
			return 0;
		}
	}

	public List<Content> selectContentListByCgid(int cgid){
		return contentMapper.selectContentListByCgid(cgid);
	}

	public List<Content> selectContentListByTid(int tid){
		String t = String.valueOf(tid);
		return contentMapper.selectContentListByTid(t);
	}

	public int updateContentViewsBySlug(String slugName){
		return contentMapper.updateContentViewsBySlug(slugName);
	}

	public Integer selectContentAuthorIdBycontentId(int contentId){
		return contentMapper.selectContentAuthorIdBycontentId(contentId);
	}

	public int selectCommentCountByCid(Integer cid){
		return contentMapper.selectCommentCountByCid(cid);
	}

	public int deleteByPrimaryKey(int cid){
		int a = contentMapper.deleteByPrimaryKey(cid);
		if (a == 1) {
			int b = commentMapper.deleteByCid(cid);
			return 1;
		} else {
			return 0;
		}
	}

	public int deleteSelectContent(String[] sids){
		try {
			if (sids != null && sids.length > 0) {
				for (String sid: sids) {
					int a = contentMapper.deleteByPrimaryKey(Integer.parseInt(sid));
					//删除所属的评论
					int b = commentMapper.deleteByCid(Integer.parseInt(sid));
				}
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return 0;
		}
		return 1;
	}

	public int selectContentOrderByCid(int parseInt){
		return contentMapper.selectContentOrderByCid(parseInt);
	}

	public int changeContentOrderByCid(int parseInt){
		return contentMapper.changeContentOrderByCid(parseInt);
	}

	public int updateContentCgidDefaultByCid(Integer cid){
		return contentMapper.updateContentCgidDefaultByCid(cid);
	}

	public int updateContentTagListByCid(Content content){
		return contentMapper.updateContentTagListByCid(content);
	}

	public String selectSlugByCid(Integer cid){
		return contentMapper.selectSlugByCid(cid);
	}

	public List<Content> selectContentListByLike(String searchWord){
		return contentMapper.selectContentListByLike(searchWord);
	}

	public List<Content> selectPostSizeContentWithHot(int postsListSize){
		return contentMapper.selectPostSizeContentWithHot(postsListSize);
	}

	public List<Content> selectContentListWithUid(int userId){
		return contentMapper.selectContentListWithUid(userId);
	}

	public int updateContent(Content content){
		return contentMapper.updateByPrimaryKeyWithBLOBs(content);
	}

	public Integer selectContentViewsBycontentId(int parseInt){
		return contentMapper.selectContentViewsBycontentId(parseInt);
	}
}
