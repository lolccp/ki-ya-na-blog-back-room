package com.rawchen.controller;

import com.rawchen.domain.Netease;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class NeteaseController extends BaseController{
    @PostMapping("/updateclouds")
    public String updateClouds(HttpServletRequest request){
        String wangyicloud = request.getParameter("wangyicloud");
        Netease netease = new Netease();
        netease.setId(1);
        netease.setNeteasecloudid(wangyicloud);
        int i = neteaseService.updateNetease(netease);
        if (i>0){
            return "redirect:/adminConfig";
        }
        return "redirect:/adminConfig";
    }
}
