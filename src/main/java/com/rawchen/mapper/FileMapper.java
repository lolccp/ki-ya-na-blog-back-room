package com.rawchen.mapper;

import com.rawchen.domain.MyFile;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface FileMapper {
    int deleteByPrimaryKey(Integer fid);

    int insert(MyFile record);

    int insertSelective(MyFile record);

	MyFile selectByPrimaryKey(Integer fid);

    int updateByPrimaryKeySelective(MyFile record);

    int updateByPrimaryKey(MyFile record);

	List<MyFile> selectAllFile();

	List<MyFile> selectFileListWithUid(int userId);
}