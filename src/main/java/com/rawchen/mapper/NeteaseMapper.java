package com.rawchen.mapper;

import com.rawchen.domain.Netease;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface NeteaseMapper {

    //查询
    Netease selectByPrimaryKey();

    //修改
    int updateByPrimaryKeySelective(Netease record);
}