package com.rawchen.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Netease {
    /**
    * 主键id
    */
    private Integer id;

    /**
    * 网易云id
    */
    private String neteasecloudid;

}