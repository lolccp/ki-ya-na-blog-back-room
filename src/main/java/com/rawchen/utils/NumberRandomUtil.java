package com.rawchen.utils;

import java.util.Random;

public class NumberRandomUtil {
    public static String ranDom() {
        Random random = new Random();
        String str = "";
        //.nextLong()方法有可能随机生成负数,要除去这种情况的
        while (true) {
            long temp = random.nextLong();
            if (temp < 0) {
                continue;
            }
            str += temp;
            if (str.length() == 17) {
                break;
            }
            //如果不是17位数字的话,要还原成最初状态
            str = "";
        }
        //首位是开始位置,默认是从0开始,substring(a,b),截取区间为 [a,b),左关右开
        str = str.substring(0, 12);
        return str;
    }
}
