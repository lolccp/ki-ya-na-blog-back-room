package com.rawchen.utils;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePrecreateRequest;
import com.alipay.api.response.AlipayTradePrecreateResponse;

public class AliPayUtil {
    public String aliPay(String tradename,Integer money,String ordernumber) throws AlipayApiException {
        AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do",
                "2021003168657511",
                "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQD2y/K9twfcJqdArszaaybTpTZZC8Hr/Kp9pOQRPy0uWt9bNPfvyePatKXuqLBMUfAG8Gv7XWcPp4lPdPjIjELWf1IwN7YoREA708tzgm+SN3GFYpGTr4EzoE8kUeAiPKwQJEZA0+D+6Tdiavq+iyOontfKshqQ+maerVPlHbN6JGIrz0IbZW8swn5ujKsQAMjmctIEoMHP2+V2KXwREOgoz0t5BVjyYawOKOVRllsW9Vne69MrSGTMbwjbhFX6V9pwiZR5ZJDEofTiBbLZdMF4Ednm7oqwuObzh+WPV9T+CgrM1lofpibplJ2giedLwkyCBYazJjGI/t8ER3NmXsSZAgMBAAECggEBAKALeotVGp/F4UitEuPCARfXV/s/CK8E13tLZytYEPCytY3sN60k+8JkzAaNFmcn68RUPXJxC9i3CPCiJoPHkcCQKLhkKJjMSeuCWbm6qKfllJSva0Ce2+ai2y/ueK87MUn2fCimPo6Qpv4H6juO97zt5XrpLRk/iTT1qiY2sKFPmN6MIF2MS0nTs3WR54a6944Q0hoMngE/TinQ70hnF72iMNa+DmxSsJmqkzx1MTG8BVd4yXGrWnNkKw7vnAi11LjaqOOMnN2n8nTELLybHRDydGlRF+MMMoWPXGRIzqumpFbtyu0TyMfqzjpuG927j2F7JUjZATlL5DsjWCtqsgECgYEA/zahljdWc2MH6T6HdQKjTUpvPpvc8QZ6kIVaY4kb2wZtRQ1oGwqSI+6ey6XVId6QHDmBhUWHjS6V4/p3OLIZaClUgxlPOJvfeuSgVKBzlunOZt6COn6azMC5XT6ZVGmKNZc8eSi2wl8VoIxqxh/GMRGF47y2F4yKXu262xkDYpECgYEA946tEEfst3fpSyOB3hSsvR14Ko72OFskR+YkBV5CldcHapLxRmuloGJC0w6P6sCRb1r5x3xWM3JkCCoQfmdWlH/ttvX86RJY5Tp7mQ5JXcFetzh7Vzr71L+mTsG22LzZXdyb0bhGKyaOXy6yBIAyexxlF6BcI5wk6ISQxHM3NYkCgYAlWWJmcGzHcCu9N7htKFirvAPxvq+j2lXfhpSG0o7wC827+E1O/8oJVieJGBjL3x1p9AR/c4B16eZnucTu2Cmds8a5dN/CIhWfj4vjNqQT5t2c8l1OoC6ZXm8CNWINF3DxjNN9y7aLVYz9tO7/VbEUh0L35Csl4Rxy7gmVfOCg8QKBgQDnYfTCPee0CCEGuPZeY4ac8FlrtdnPDOkZRrMuTU8BDE2DQDrBbkYNZSo+aOL9MOX+ZdMGsGFFjsqOkvp9mwrim7vNAZhVJG/wOTxi72aFegKUT/eArcHJtSO1gvvrEoA8acE+QPVcyZnSvytmZ4hHZ1/ZuGnGYuBgb997rpDQuQKBgFzKDMyxgCnFv6GaP0lu6eALtQbWeOJo9K9yOAC0VWqKpCGGhD2Y2p+Pv4+UPjhwfxnBMLuZCkaJ5Bp/Ekcjial6CjGdd+HtiR/RX7VdJJzPIPnM8GDebxF9W3ucs0lRT1BkXTDeGIu1l3dBC1dAzt8k0tXulwifloq4L3l+XdcZ",
                "json","UTF-8",
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkmYhj/HbexRVueN7e/35w1EO87JRmWoDiiVcaXSndrJvsBdjI7lHcGrHH6GvG/RV9Zszoof9eniy0iDZkbv5dgJhNrXjAUNiHmCGOAjyBBK4XJWzqzCRDl8ulUjnxwUIz953BJqdKr7zaQ4dtqQBH54d1MuRfSOqoICTKlCcdAwaZfcFbhF92AJcz8+ekn/wr0GN1HOCyeHrQXMgl+HQYV/UBZQxvO+raVI7Ut/sU1AjHls3JNMVe9mxrgCzleprtIhbP3FQfzlp+gTf5cnHVcU9+F1tVE6IBwJo4WF24xs12x+9mLJzhGnoIVcYuQOF9+UhNH4gDtIlfqftzF9RcwIDAQAB",
                "RSA2");
        AlipayTradePrecreateRequest request = new AlipayTradePrecreateRequest();
        request.setNotifyUrl("");
        JSONObject bizContent = new JSONObject();
        bizContent.put("out_trade_no", ordernumber);
        bizContent.put("total_amount",money);
        bizContent.put("subject", tradename);
        request.setBizContent(bizContent.toString());
        AlipayTradePrecreateResponse response = alipayClient.execute(request);
        System.out.println();
        if(response.isSuccess()){
            System.out.println("调用成功");
        } else {
            System.out.println("调用失败");
        }
        return response.getBody();
    }
}
