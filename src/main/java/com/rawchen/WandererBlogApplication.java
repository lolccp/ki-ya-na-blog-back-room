package com.rawchen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WandererBlogApplication {

    public static void main(String[] args) {
        SpringApplication.run(WandererBlogApplication.class, args);
    }

}
