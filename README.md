# KiYaNaBlogBackRoom——又是一个博客系统


#### 软件架构
springboot+mybatis+templates+mysql+maven


#### 安装教程

1.  直接放入idea中让idea进行对jar的下载
2.  下载完成后将你的数据库密码和数据库在yml中进行更改
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 你的名字xxxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  本博客使用了java进行开发
2.  本系统遵循开源原则
3.  你可以随意更改本博客中的所有内容
4.  你可以可以加入我们进行对这个博客的二次开发并提交你自己的分支
